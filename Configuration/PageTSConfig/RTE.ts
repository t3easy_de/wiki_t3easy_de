RTE.classes{
	fe-hide {
		name = Im Frontend verbergen
		value = color:#CCCCCC;font-style:italic;
	}
	code {
		name = Code
		value = font-family: 'Courier New', Courier, monospace; color: #0EFC57; background-color:#0E0E0E; padding: .1em .2em;
	}
}
RTE.default {
	contentCSS.wiki_t3easy_de = EXT:wiki_t3easy_de/Resources/Public/Stylesheets/RTE.css
	showButtons := addToList(spellcheck)
	defaultContentLanguage = de
	buttons.formatblock.removeItems := addToList(h1)
	buttons.textstyle.tags.span.allowedClasses := addToList(fe-hide,code)
	proc.allowedClasses := addToList(fe-hide,code)

	# spellcheck
	showButtons := addToList(spellcheck)
	buttons.spellcheck.enablePersonalDictionaries = 1
	buttons.spellcheck.dictionaries.restrictToItems = de,en
}
