plugin.tx_news {
	settings {
		detail.showSocialShareButtons = 0
		displayDummyIfNoMedia = 0
		cropMaxCharacters = 300
		opengraph.site_name = wiki.t3easy.de
	}
}
