<?php
defined('TYPO3_MODE') or die();

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/PageTSConfig/RTE.ts',
            'RTE settings'
        );
    },
    'wiki_t3easy_de'
);
