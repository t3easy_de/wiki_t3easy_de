<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Template package for wiki.t3easy.de',
    'description' => '',
    'category' => 'distribution',
    'author' => 'Jan Kiesewetter',
    'author_email' => 'jan@t3easy.de',
    'author_company' => 't3easy',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'alpha',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'version' => '1.0.0-dev',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-7.6.99',
            'basepackage' => '7.6.0-7.6.99'
        ],
        'conflicts' => [],
        'suggests' => []
    ]
];
